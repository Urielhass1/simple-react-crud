export const FETCH_USERS = 'fetch_users';
export const CREATE_USER = 'create_user';
export const FETCH_USER = 'fetch_user';
export const DELETE_USER = 'delete_user';
export const UPDATE_USER = 'update_user';

export function updateUser(values) {
    return {
        type: UPDATE_USER,
        payload: values
    }
}

export function fetchUsers() {
    return {
        type: FETCH_USERS
    };
}


export function addUser(values, callback) {
    callback();
    return {
        type: CREATE_USER,
        payload: values
    };
}

export function fetchUser(id) {
    return {
        type: FETCH_USER,
        payload: []
    };
}

export function deleteUser(id, callback) {
    callback();
    return {
        type: DELETE_USER,
        payload: id
    };
}
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Form from './Form';

class Add extends Component {
  render() {
    return (
      <div className="userAdd card">
        <div className="card-header d-flex justify-content-between">
          <h3>Novo usuário</h3>
          <Link className="btn btn-link" to="/">
            Voltar para a listagem
          </Link>
        </div>
        <div className="card-body">
          <Form history={this.props.history} />
        </div>
      </div>
    );
  }
}
export default connect()(Add);
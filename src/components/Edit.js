import React, { Component } from 'react';
import { connect } from 'react-redux';
import Form from './Form';
import { Link } from 'react-router-dom';
import { fetchUser, deleteUser } from '../actions';

class Edit extends Component {
  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.fetchUser(id);
  }

  onDeleteClick() {
    const { id } = this.props.match.params;
    this.props.deleteUser(id, () => {
      this.props.history.push('/');
    });
  }
  
  render() {
    const { user } = this.props;
    if (!user) {
        return <div>Loading...</div>;
    }
    return (
      <div className="card">
        <div className="card-header">
          <div className="d-flex justify-content-between">
            <h3>
              Editar usuário: <strong>{user.name}</strong>
            </h3>
            <div className="d-flex justify-content-end">
              <Link to="/" className="btn btn-link">
                Voltar para a listagem
              </Link>
              <button
                  className="btn btn-danger ml-2"
                  onClick={this.onDeleteClick.bind(this)}
              >
                Delete
              </button>
            </div>
          </div>
        </div>
        <div className="card-body">
          <Form user={user} history={this.props.history} />
        </div>
      </div>
    );
  }
}

function mapStateToProps({ users }, ownProps) {
  return { user: users[ownProps.match.params.id] };
}

export default connect(mapStateToProps, { fetchUser, deleteUser})(Edit);
import { connect } from 'react-redux';
import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { addUser } from '../actions';

class Form extends Component {
  
  componentDidMount() {
    const user = this.props.user;
    if (user && user !== undefined) {
      this.props.initialize({
        name: user.name,
        email: user.email,
        username: user.username,
        id: user.id
      });
    }
  }
  
  renderField(field) {
    const { meta: { touched, error } } = field;
    const className = `form-group ${touched && error ? 'has-danger ' : ' ' } ${field.type == 'hidden' ? 'd-none' : '' }`

    return (
      <div className={className}>
        <label>{field.label}</label>
        <input 
          className="form-control"
          type={field.type}
          {...field.input}
        />
        <div className="text-help">
          {touched ? error : '' }
        </div>
      </div>
    );
  }
    
  onSubmit(values) {
    if (!values.id) {
      values.id = '_' + Math.random().toString(36).substr(2, 9);
    }
    this.props.addUser(values, () => {
      this.props.history.push('/');
    });
  }
    
  render () {
    const { handleSubmit } = this.props;
    return (
      <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
        <Field 
          name="name"
          label="Nome"
          type="text"
          component={this.renderField}
        />
        <Field 
          name="email"
          label="Email"
          type="email"
          component={this.renderField}
        />
        <Field 
          name="username"
          label="Username"
          type="text"
          component={this.renderField}
        />
        <Field 
          name="id"
          type="hidden"
          component={this.renderField}
        />

        <button type="submit" className="btn btn-primary">
          { !this.props.user ? 'Adicionar' : 'Editar' }
        </button>
        <Link to="/" className="btn btn-secondary ml-2">Cancelar</Link>
      </form>
      );
    }
}
function validate(values) {

  const errors = {};
  if (values.name && values.name.length < 3) {
      errors.name =  'Nome preciso ter no mínio 3 carácter';
  }

  if (!values.name) {
      errors.name = 'Digite um nome!';
  }

  if (!values.email) {
      errors.email = 'Digite algum email.';
  }

  if (!values.username) {
      errors.username = 'Digite algum username por favor.';
  }

  return errors;
}

export default reduxForm({
    form: 'Form',
    validate,
})(
    connect(null, { addUser }) (Form)
);
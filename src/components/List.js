import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchUsers } from '../actions';

class List extends Component {
    componentDidMount() {
      this.props.fetchUsers();
    }

    renderUsers() {
      return _.map(this.props.users, user => {
        if (user) {
          return (
            <li className="list-group-item" key={user.id}>
              <Link to={`/usuarios/${user.id}`} >
                {user.name}
              </Link>
            </li>
          );
        }
      });
    }

  render() {
    return (
      <div className="card">
        <div className="card-header d-flex justify-content-between">
          <h3>
            Usuários
          </h3>
          <Link className="btn btn-primary" to="/usuarios/novo">
            Novo Usuário
          </Link>
        </div>
        <ul className="list-group">
          { this.renderUsers() }
        </ul>
      </div>
    );
  }
}


function mapStateToProps(state) {
  return { users: state.users };
}


export default connect(mapStateToProps, { fetchUsers })(List);
import _ from 'lodash';
import { FETCH_USERS, FETCH_USER, DELETE_USER, CREATE_USER, UPDATE_USER } from '../actions';

export default function(state = [], action) {
  // const data = {
  //   id: '_7ajua69fw',
  //   name: 'Uriel Hass',
  //   username: 'uriel.hass',
  //   email: 'hass.uriel@gmail.com'
  // };
  // state = {
  //   ...state,
  //     [data.id]: data
  // }
  switch (action.type) {
    //sem utilidade para usar nesse caso.
    case UPDATE_USER:
      return state.map((user) => {
        if (user.id === action.id) {
          return {
            ...user,
            name: action.payload.name,
            email: action.payload.email,
            username: action.payload.username,
            editing: !user.editing
          }
        } else {
          return user; 
        }
      })

    case DELETE_USER:
        return _.omit(state, action.payload);

    case FETCH_USER:
        return { 
          ...state, 
          [action.payload.id]: action.payload.data 
        }

    case FETCH_USERS:
        return state;

    case CREATE_USER:
      return {
        ...state,
        [action.payload.id]: action.payload
      };
    default:
        return state;
  }
}